(ns plffinal.core-test
  (:require [clojure.test :refer :all]
            [plffinal.core :refer :all]
            [plffinal.ejercicio1 :as ejercicio1]
            [plffinal.ejercicio2 :as ejercicio2]
            [plffinal.ejercicio3 :as ejercicio3]))

(deftest regresa-al-punto-de-origen?-test
  (testing "Pruebas unitarias del ejercicio 1"
    (is (true? (ejercicio1/regresa-al-punto-de-origen? "")))
    (is (true? (ejercicio1/regresa-al-punto-de-origen? [])))
    (is (true? (ejercicio1/regresa-al-punto-de-origen? (list))))
    (is (true? (ejercicio1/regresa-al-punto-de-origen? (list \> \<))))
    (is (true? (ejercicio1/regresa-al-punto-de-origen? "v^")))
    (is (true? (ejercicio1/regresa-al-punto-de-origen? [\v \^])))
    (is (true? (ejercicio1/regresa-al-punto-de-origen? "^>v<")))
    (is (true? (ejercicio1/regresa-al-punto-de-origen? (list \^ \> \v \<))))
    (is (true? (ejercicio1/regresa-al-punto-de-origen? "<<vv>>^^")))
    (is (true? (ejercicio1/regresa-al-punto-de-origen? [\< \< \v \v \> \> \^ \^])))

    (is (false? (ejercicio1/regresa-al-punto-de-origen? ">")))
    (is (false? (ejercicio1/regresa-al-punto-de-origen? (list \>))))
    (is (false? (ejercicio1/regresa-al-punto-de-origen? "<^")))
    (is (false? (ejercicio1/regresa-al-punto-de-origen? [\< \^])))
    (is (false? (ejercicio1/regresa-al-punto-de-origen? ">>><<")))
    (is (false? (ejercicio1/regresa-al-punto-de-origen? (list \> \> \> \< \<))))
    (is (false? (ejercicio1/regresa-al-punto-de-origen? [\v \v \^ \^ \^])))))

(deftest regresan-al-punto-de-origen?-test
  (testing "Pruebas unitarias del ejercicio 2"
    (is (true? (ejercicio2/regresan-al-punto-de-origen?)))
    (is (true? (ejercicio2/regresan-al-punto-de-origen? [])))
    (is (true? (ejercicio2/regresan-al-punto-de-origen? "")))
    (is (true? (ejercicio2/regresan-al-punto-de-origen? [] "" (list))))
    (is (true? (ejercicio2/regresan-al-punto-de-origen? "" "" "" "" [] [] [] (list) "")))
    (is (true? (ejercicio2/regresan-al-punto-de-origen? ">><<" [\< \< \> \>] (list \^ \^ \v \v))))

    (is (false? (ejercicio2/regresan-al-punto-de-origen? (list \< \>) "^^" [\> \<])))
    (is (false? (ejercicio2/regresan-al-punto-de-origen? ">>>" "^vv^" "<<>>")))
    (is (false? (ejercicio2/regresan-al-punto-de-origen? [\< \< \> \> \> \> \> \> \> \>])))))

(deftest regreso-al-punto-de-origen-test
  (testing "Pruebas unitarias del ejercicio 3"
    (is (= () (ejercicio3/regreso-al-punto-de-origen "")))
    (is (= () (ejercicio3/regreso-al-punto-de-origen (list \^ \^ \^ \> \< \v \v \v))))
    (is (= (seq [\< \< \<]) (ejercicio3/regreso-al-punto-de-origen ">>>")))
    (is (= (seq [\< \< \^ \^ \^ \>]) (ejercicio3/regreso-al-punto-de-origen [\< \v \v \v \> \>])))))
