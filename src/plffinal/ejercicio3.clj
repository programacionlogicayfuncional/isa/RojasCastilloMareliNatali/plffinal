(ns plffinal.ejercicio3
  (:require [plffinal.core :as patron]
            [plffinal.ejercicio1 :as referencia]
            [clojure.test :refer :all]))

(defn regreso-al-punto-de-origen
  [x]
  (if (referencia/regresa-al-punto-de-origen? x)
    ()
    (reverse (apply str (apply concat (map repeat
                                           (vals (patron/movimientos-contrarios x))
                                           (keys (patron/movimientos-contrarios x))))))))




;-----RESPUESTAS  ()


(regreso-al-punto-de-origen "")
(regreso-al-punto-de-origen (list \^ \^ \^ \> \< \v \v \v))

;-----RESPUESTAS recorrido
(regreso-al-punto-de-origen ">>>")

(regreso-al-punto-de-origen [\< \v \v \v \> \>])
