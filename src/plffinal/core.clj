(ns plffinal.core)

(defn pertenencia
  [s]
  (vec (map
        (zipmap
         [\< \v \> \^]
         [\> \^ \< \v])
        s)))

(defn demostrando
  [x]
  (cond
    (empty? x) [0 0]
    (map int? x) x))

(defn contar-movimientos
  [a] (frequencies (vec a)))

(defn movimientos-contrarios
  [a] (frequencies (pertenencia a)))

