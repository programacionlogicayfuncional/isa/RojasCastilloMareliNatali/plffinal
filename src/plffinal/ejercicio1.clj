(ns plffinal.ejercicio1
  (:require [plffinal.core :as patron]
            [clojure.test :refer :all]))

(defn regresa-al-punto-de-origen?
  [c]
  (let [a (patron/contar-movimientos c)
        b (patron/movimientos-contrarios c)
        d (keys a)
        e (keep a d)
        f (keep b d)
        h (patron/demostrando f)]
    (or (empty? c) (every? true? (map == e h)))))







;-----RESPUESTAS TRUE


(regresa-al-punto-de-origen? "")
(regresa-al-punto-de-origen? [])
(regresa-al-punto-de-origen? (list))

(regresa-al-punto-de-origen? "><")
(regresa-al-punto-de-origen? (list \> \<))
(regresa-al-punto-de-origen? "v^")
(regresa-al-punto-de-origen? [\v \^])
(regresa-al-punto-de-origen? "^>v<")
(regresa-al-punto-de-origen? (list \^ \> \v \<))
(regresa-al-punto-de-origen? "<<vv>>^^")
(regresa-al-punto-de-origen? [\< \< \v \v \> \> \^ \^])

;-----RESPUESTAS FALSE

(regresa-al-punto-de-origen? ">")
(regresa-al-punto-de-origen? (list \>))
(regresa-al-punto-de-origen? "<^")
(regresa-al-punto-de-origen? [\< \^])
(regresa-al-punto-de-origen? ">>><<")
(regresa-al-punto-de-origen? (list \> \> \> \< \<))
(regresa-al-punto-de-origen? [\v \v \^ \^ \^])

