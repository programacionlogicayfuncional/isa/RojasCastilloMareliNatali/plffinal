(ns plffinal.ejercicio2
  (:require [plffinal.ejercicio1 :as referencia]
            [clojure.test :refer :all]))

(defn regresan-al-punto-de-origen?
  [& args]
  (every? true? (map referencia/regresa-al-punto-de-origen? args)))







;-----RESPUESTAS TRUE


(regresan-al-punto-de-origen?)
(regresan-al-punto-de-origen? "")
(regresan-al-punto-de-origen? [])
(regresan-al-punto-de-origen? [] "" (list))
(regresan-al-punto-de-origen? "" "" "" "" [] [] [] (list) "")
(regresan-al-punto-de-origen? ">><<" [\< \< \> \>] (list \^ \^ \v \v))
;-----RESPUESTAS FALSE
(regresan-al-punto-de-origen? (list \< \>) "^^" [\> \<])
(regresan-al-punto-de-origen? ">>>" "^vv^" "<<>>")
(regresan-al-punto-de-origen? [\< \< \> \> \> \> \> \> \> \>])

